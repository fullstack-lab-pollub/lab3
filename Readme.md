Aby uruchomić wielokontenerowego pod-a o nazwie sidecar-pod, który będzie
działał w przestrzeni nazw lab3, oraz będzie spełniał poniższe założenia:
- w podstawowym kontenerze działa busybox i zapisuje dane wyjściowe za pomocą
polecnenia date do pliku /var/log/date.log co 5 sekund,
- drugi kontener działa jako kontener typu sidecar, który zawiera serwer
nginx. Serwer ten zapewnia dostęp do tego pliku /var/log/date.log przy
użyciu woluminu współdzielonego typu hostPath zamontowanego w 
/usr/share/nginx/html
należy użyć następujących poleceń:
kubectl apply -f lab3-namespace.yaml
kubectl apply -f sidecar-pod.yaml
Oczywiście przed uruchomieniem powyższych poleceń, należy mieć odpowiednio
utworzone pliki .yaml.

Następnie w celu sprawdzenia poprawności działania należy uruchomić poniższe
polecnie:
kubectl port-forward -n lab3 sidecar-pod 8080:80

W innym oknie należy następnie sprawdzić dostępność pliku /var/log/date.log 
za pomoca polecenia curl:
curl http://localhost:8080/date.log

Rezultat wykonania powyższych poleceń:
![](/lab3_1.png)
![](/lab3_2.png)
![](/lab3_3.png)
